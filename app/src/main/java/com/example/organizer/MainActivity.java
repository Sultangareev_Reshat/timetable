package com.example.organizer;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;


public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecycleView;
    private dayObjectAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<dayObject> days = new ArrayList<>();
    private ArrayList<dayObject> loadedDays;
    private String sharedPref = "Shared preferences11";
    private String sharedKey = "key1";
    private Context mcontext = this;

    private int lastexpaned = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        loadData();
        syncDay();

        setUp();
        setAdapterOnClickListener();
//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    protected void onDestroy() {
        syncForLoadDay();
        saveData();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        syncForLoadDay();
        saveData();
        super.onPause();
    }


    public void setUp() {
        mRecycleView = findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new dayObjectAdapter(days);
        mRecycleView.setLayoutManager(mLayoutManager);
        mRecycleView.setAdapter(mAdapter);
    }

    public void setAdapterOnClickListener() {
        mAdapter.setOnItemClickListener(new dayObjectAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                boolean expanded = days.get(position).isExpanded();
                days.get(position).setExpanded(!expanded);
                mAdapter.notifyItemChanged(position);
                if (lastexpaned != -1 && lastexpaned != position) {
                    days.get(lastexpaned).setExpanded(false);
                    mAdapter.notifyItemChanged(lastexpaned);
                }
                lastexpaned = position;
            }
            @Override
            public void onDeleteClick(int position) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                removeItem(position);

                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }

            @Override
            public void onAddClick(int position) {
                addItem(position);
            }

            @Override
            public void onTimeChange(int position, String text) {
                days.get(position).changeTextTime(text);
                //mAdapter.notifyItemChanged(position);
            }
            @Override
            public void onRoomChange(int position, String text) {
                days.get(position).changeTextRoom(text);
                //mAdapter.notifyItemChanged(position);
            }

            @Override
            public void onSubjectChange(int position, String text) {
                days.get(position).changeTextSubject(text);
                //mAdapter.notifyItemChanged(position);
            }
        });
    }

    public void removeItem(int position) {
        days.remove(position);
        mAdapter.notifyItemRemoved(position);
    }

    public void addItem(int position) {
        Calendar calendar = Calendar.getInstance();
        int day2 = calendar.get(Calendar.DAY_OF_YEAR);
        int dayOfWeek = 0;
        if (((day2 - 34) % 14) > 7) {
            dayOfWeek = ((day2 - 34) % 14);
        } else {
            dayOfWeek = ((day2 - 34) % 14);
        }

        int len = days.size();
        for (int i = 1; i < len; i++) {
            if (days.get(i).isHeader() && i < position) {
                dayOfWeek++;
            }
        }
        days.add(position, new dayObject("Subject", "Room", "Time", dayOfWeek % 14));
        mAdapter.notifyItemInserted(position);
    }

    public void addLoadedDays() {
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Monday", 0));


        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Tuesday", 1));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Wednesday", 2));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Thursday", 3));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Friday", 4));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Saturday", 5));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Sunday", 6));

        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Monday", 7));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Tuesday", 8));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Wednesday", 9));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Thursday", 10));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Friday", 11));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Saturday", 12));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Sunday", 13));
    }
    public void addSchedule() {
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Monday", 0));
        loadedDays.add(new dayObject("Операционные системы", "5423", "9:50", 0));
        loadedDays.add(new dayObject("Физкультура", "----", "11:40", 0));

        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Tuesday", 1));
        loadedDays.add(new dayObject("Экономика Организации(л)", "5405", "9:50", 1));
        loadedDays.add(new dayObject("Схемотехника(п)", "1154", "11:40", 1));
        loadedDays.add(new dayObject("ИТ(п)", "1211", "13:45", 1));
        loadedDays.add(new dayObject("Экономика Организации(п)", "5205", "15:35", 1));

        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Wednesday", 2));
        loadedDays.add(new dayObject("ТВиМС(л)", "3324", "9:50", 2));
        loadedDays.add(new dayObject("ОС(п)", "1214", "11:40", 2));

        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Thursday", 3));
        loadedDays.add(new dayObject("Физра", "----", "8:00", 3));
        loadedDays.add(new dayObject("ИТ(л)", "5423", "9:50", 3));
        loadedDays.add(new dayObject("Английский язык", "----", "11:40", 3));

        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Friday", 4));
        loadedDays.add(new dayObject("ТВиМС(п)", "3428", "9:50", 4));
        loadedDays.add(new dayObject("КП(л)", "3324", "11:40", 4));
        loadedDays.add(new dayObject("КП(п)", "1214", "13:45", 4));


        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Saturday", 5));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Sunday", 6));

        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Monday", 7));
        loadedDays.add(new dayObject("Операционные системы", "5423", "9:50", 7));
        loadedDays.add(new dayObject("Физкультура", "----", "11:40", 7));
        loadedDays.add(new dayObject("Схемотехника(л)", "1158", "13:45", 7));

        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Tuesday", 8));
        loadedDays.add(new dayObject("Экономика Организации(л)", "5405", "9:50", 8));
        loadedDays.add(new dayObject("Схемотехника(л)", "1154", "11:40", 8));
        loadedDays.add(new dayObject("ИТ(п)", "1211", "13:45", 8));
        loadedDays.add(new dayObject("Экономика Организации(п)", "5205", "15:35", 8));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Wednesday", 9));
        loadedDays.add(new dayObject("ТВиМС(л)", "3324", "9:50", 9));
        loadedDays.add(new dayObject("ОС(п)", "1214", "11:40", 9));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Thursday", 10));
        loadedDays.add(new dayObject("Физра", "----", "8:00", 10));
        loadedDays.add(new dayObject("ИТ(л)", "5423", "9:50", 10));
        loadedDays.add(new dayObject("Английский язык", "----", "11:40", 10));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Friday", 11));
        loadedDays.add(new dayObject("ТВиМС(п)", "3428", "9:50", 11));
        loadedDays.add(new dayObject("КП(л)", "3324", "11:40", 11));
        loadedDays.add(new dayObject("КП(п)", "1214", "13:45", 11));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Saturday", 12));
        loadedDays.add(new dayObject("Физра1", "с10201", "8-15", "Sunday", 13));
    }

    public void changeItem(int position, String text) {
        days.get(position).changeTextRoom(text);
        mAdapter.notifyItemChanged(position);
    }

    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(sharedPref, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(loadedDays);
        editor.putString(sharedKey, json);
        editor.apply();
    }

    private void syncForLoadDay() {
        loadedDays.clear();
        int k = 0;
        for (int t = 0; t < days.size(); ++t) {
            for (int i = 0; i < days.size(); ++i) {
                if (days.get(i).getDayOfTheWeek() == k) {
                    loadedDays.add(days.get(i));
                }
            }
            ++k;
        }
//        for (int i = 0; i < loadedDays.size(); ++i)
//            System.out.println("LoadDay is" + loadedDays.get(i).getDayOfTheWeek() + " " + loadedDays.get(i).getSubject());

    }

    private void syncDay() {
        Calendar calendar = Calendar.getInstance();
        int day2 = calendar.get(Calendar.DAY_OF_YEAR);
        int dayOfWeek = (day2 - 34) % 14;

        for (int i = 0; i < loadedDays.size(); ++i) {
            if (loadedDays.get(i).getDayOfTheWeek() >= dayOfWeek) {
                if (!loadedDays.get(i).isHeader())
                    days.add(new dayObject(loadedDays.get(i).getSubject(), loadedDays.get(i).getRoom(), loadedDays.get(i).getTime(), loadedDays.get(i).getDayOfTheWeek()));
                else {
                    days.add(new dayObject(loadedDays.get(i).getSubject(), loadedDays.get(i).getRoom(), loadedDays.get(i).getTime(), loadedDays.get(i).getHeader(), loadedDays.get(i).getDayOfTheWeek()));
                }
            }
        }
        for (int i = 0; i < loadedDays.size(); ++i) {
            if (loadedDays.get(i).getDayOfTheWeek() < dayOfWeek) {
                if (!loadedDays.get(i).isHeader())
                    days.add(new dayObject(loadedDays.get(i).getSubject(), loadedDays.get(i).getRoom(), loadedDays.get(i).getTime(), loadedDays.get(i).getDayOfTheWeek()));
                else {
                    days.add(new dayObject(loadedDays.get(i).getSubject(), loadedDays.get(i).getRoom(), loadedDays.get(i).getTime(), loadedDays.get(i).getHeader(), loadedDays.get(i).getDayOfTheWeek()));
                }
            }
        }
//        for (int i = 0; i < 14; ++i) {
//            System.out.println(days.get(i).getDayOfTheWeek() + "INFO " + days.get(i).getSubject());
//        }
    }

    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(sharedPref, MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(sharedKey, null);
        Type type = new TypeToken<ArrayList<dayObject>>() {
        }.getType();

        //addLoadedDays();

        loadedDays = gson.fromJson(json, type);

        if (loadedDays == null) {
            System.out.println("INSIDE OF NEW");
            loadedDays = new ArrayList<>();
            addLoadedDays();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            loadedDays.clear();
            int i = 0;
            while (!days.isEmpty())
            {
                removeItem(0);
            }
            addSchedule();
            System.out.println("Here");
            syncDay();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.example.organizer;

import androidx.constraintlayout.widget.ConstraintLayout;

public class dayObject {
    private String dtextSubject;
    private String dtextRoom;
    private String dtextTime;
    private boolean expanded;
    private String dheader;
    private boolean disHeader;
    private int dayOfTheWeek;


    public dayObject(String textSubject, String textRoom, String textTime, String header,int dayOfW)
    {
        dtextSubject = textSubject;
        dtextRoom = textRoom;
        dtextTime = textTime;

        dayOfTheWeek = dayOfW;
        dheader = header;
        disHeader = true;
    }
    public dayObject(String textSubject, String textRoom, String textTime, int dayOfW)
    {
        dtextSubject = textSubject;
        dtextRoom = textRoom;
        dtextTime = textTime;

        dayOfTheWeek = dayOfW;
        disHeader = false;
    }
    public void setExpanded(boolean setter){
        expanded = setter;
    }
    public boolean isExpanded(){
        return expanded;
    }
    public void changeTextSubject(String text){
        dtextSubject = text;
    }
    public void changeTextRoom(String text){
        dtextRoom = text;
    }
    public void changeTextTime(String text){
        dtextTime = text;
    }
    public String getHeader(){ return dheader;}
    public boolean isHeader(){
        return disHeader;
    }
    public int getDayOfTheWeek(){
        return dayOfTheWeek;
    }
    public String getSubject()
    {
        return dtextSubject;
    }
    public String getRoom()
    {
        return  dtextRoom;
    }
    public String getTime()
    {
        return  dtextTime;
    }

}

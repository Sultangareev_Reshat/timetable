package com.example.organizer;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class dayObjectAdapter extends RecyclerView.Adapter<dayObjectAdapter.DayObjectViewHolder> {
    private ArrayList<dayObject> mExampleList;
    private  OnItemClickListener mListener;

    public  interface  OnItemClickListener{
        void onItemClick(int position);
        void onDeleteClick(int position);
        void onAddClick(int position);
        void onTimeChange(int position, String text);
        void onRoomChange(int position, String text);
        void onSubjectChange(int position, String text);
    }
    public void setOnItemClickListener(OnItemClickListener listener)
    {
        mListener = listener;
    }

    public static class  DayObjectViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextSubject;
        public TextView mTextViewRoom;
        public TextView mTextViewTime;
        public TextView mTextViewDate;
        public EditText mTextViewTimeEdit;
        public EditText mTextViewRoomEdit;
        public EditText mTextViewSubjectEdit;
        public TextView mHeader;

        public ImageView mAddText;
        public ImageView mDeleteImage;
        public ConstraintLayout mConstraintLayout;

        public  DayObjectViewHolder(final View itemView, final OnItemClickListener listener) {
            super(itemView);
            mTextSubject = itemView.findViewById(R.id.textViewSubject);
            mTextViewRoom = itemView.findViewById(R.id.textViewRoom);
            mTextViewTime = itemView.findViewById(R.id.textViewTime);
            mTextViewTimeEdit = itemView.findViewById(R.id.editTextTime);
            mTextViewRoomEdit = itemView.findViewById(R.id.editTextRoom);
            mTextViewSubjectEdit = itemView.findViewById(R.id.editTextSubject);
            mDeleteImage = itemView.findViewById(R.id.deleteImage);
            mAddText = itemView.findViewById(R.id.addImage);
            mConstraintLayout = itemView.findViewById(R.id.ConstraintLayout);
            mHeader = itemView.findViewById(R.id.textViewHeader);
            mTextViewDate = itemView.findViewById(R.id.textViewDate);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);

                        }
                    }
                }
            });
            mTextViewTimeEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int position = getAdapterPosition();
                    listener.onTimeChange(position, s.toString());

                }
            });
            mTextViewRoomEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int position = getAdapterPosition();
                    listener.onRoomChange(position, s.toString());
                }
            });
            mTextViewSubjectEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int position = getAdapterPosition();
                    listener.onSubjectChange(position, s.toString());
                }
            });

            mDeleteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onDeleteClick(position);

                        }
                    }
                }
            });
            mAddText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onAddClick(position + 1);
                        }
                    }
                }
            });
        }

        private void bind(dayObject day) {
            boolean expanded = day.isExpanded();
            if(day.isHeader())
            {
                mTextViewDate.setVisibility(View.VISIBLE);
                mTextSubject.setVisibility(View.GONE);
                mTextViewRoom.setVisibility(View.GONE);
                mTextViewTime.setVisibility(View.GONE);

                mTextViewTimeEdit.setVisibility(View.GONE);
                mTextViewRoomEdit.setVisibility(View.GONE);
                mTextViewSubjectEdit.setVisibility(View.GONE);

                mAddText.setVisibility(expanded ? View.VISIBLE : View.GONE);
                mDeleteImage.setVisibility(View.GONE);

                if(day.getDayOfTheWeek() <= 6)
                mHeader.setText(day.getHeader() + "(odd)");
                if(day.getDayOfTheWeek() >= 7)
                    mHeader.setText(day.getHeader() + "(even)");
                mHeader.setVisibility(View.VISIBLE);

            }
            else {
                mTextViewDate.setVisibility(View.GONE);
                mTextViewTime.setVisibility(expanded ? View.GONE : View.VISIBLE);
                mTextViewRoom.setVisibility(expanded ? View.GONE : View.VISIBLE);
                mTextSubject.setVisibility(expanded ? View.GONE : View.VISIBLE);

                mTextViewTimeEdit.setVisibility(expanded ? View.VISIBLE : View.GONE);
                mTextViewTimeEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);

                mTextViewRoomEdit.setVisibility(expanded ? View.VISIBLE : View.GONE);
                mTextViewRoomEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);

                mTextViewSubjectEdit.setVisibility(expanded ? View.VISIBLE : View.GONE);
                mTextViewSubjectEdit.setImeOptions(EditorInfo.IME_ACTION_DONE);

                mDeleteImage.setVisibility(expanded ? View.VISIBLE : View.GONE);
                mAddText.setVisibility(expanded ? View.VISIBLE : View.GONE);

                mHeader.setVisibility(View.GONE);
            }

        }
    }




    public dayObjectAdapter(ArrayList<dayObject> exampleList) {
        mExampleList = exampleList;
    }
    @Override
    public DayObjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.day, parent, false);
        DayObjectViewHolder evh = new  DayObjectViewHolder(v, mListener);
        return evh;
    }

    public String GetDate(dayObject currentItem)
    {
        Calendar cal = Calendar.getInstance();
        int day2 = cal.get(Calendar.DAY_OF_YEAR);
        if(currentItem.getDayOfTheWeek() - (day2 - 34)%14 > 0 ||currentItem.getDayOfTheWeek() ==  (day2 - 34)%14) {
            cal.add(Calendar.DAY_OF_YEAR, currentItem.getDayOfTheWeek() - (day2 - 34) % 14);
        }
        else
            cal.add(Calendar.DAY_OF_YEAR, currentItem.getDayOfTheWeek() - (day2 - 34) % 14 + 14);

        Date date=cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy");
        String formattedDate=dateFormat.format(date);
        return formattedDate;
    }
    @Override
    public void onBindViewHolder(DayObjectViewHolder holder, int position) {
        final dayObject currentItem = mExampleList.get(position);

        holder.mTextSubject.setText(currentItem.getSubject());
        holder.mTextViewRoom.setText(currentItem.getRoom());
        holder.mTextViewTime.setText(currentItem.getTime());

        holder.mTextViewDate.setText(GetDate(currentItem));

        holder.mTextViewTimeEdit.setText(currentItem.getTime(), TextView.BufferType.EDITABLE);
        holder.mTextViewRoomEdit.setText(currentItem.getRoom(), TextView.BufferType.EDITABLE);
        holder.mTextViewSubjectEdit.setText(currentItem.getSubject(), TextView.BufferType.EDITABLE);

        holder.bind(currentItem);

    }
    @Override
    public int getItemCount() {
        return mExampleList.size();
    }
}
